package pl.sda.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity
@Embeddable
@Table(name="person")
public class Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id",unique = true)
    private int id;

    @Column(name="name",nullable = false)
    private String name;

    @Column(name="surname",nullable = false)
    private String surname;

    @Column(name="age",nullable = false)
    private int age;

    @Column(name="height",nullable = false)
    private int height;

    @Column(name="weight",nullable = false)
    private Double weight;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "city", column = @Column(name = "city")),
            @AttributeOverride(name = "zipCode", column = @Column(name = "zipCode")),
            @AttributeOverride(name = "street", column = @Column(name = "street")),
            @AttributeOverride(name = "homeNumber", column = @Column(name = "homeNumber")),
    })
    private Address address;
}
