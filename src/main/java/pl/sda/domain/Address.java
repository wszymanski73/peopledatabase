package pl.sda.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity
@Embeddable
@Table(name="address")
public class Address implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id",unique = true)
    private int id;

    @Column(name="city",nullable = false)
    private String city;

    @Column(name="zipCode",nullable = false)
    private String zipCode;

    @Column(name="street",nullable = false)
    private String street;

    @Column(name="homeNumber",nullable = false)
    private int homeNumber;

}
