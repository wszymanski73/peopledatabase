package pl.sda.dao;

import pl.sda.domain.Person;

import javax.persistence.*;
import java.util.List;
import java.util.Optional;

public class PeopleDaoJPAImpl implements PeopleDao {

    private static final EntityManagerFactory emf;

    static {
        emf = Persistence.createEntityManagerFactory("jpa_person");//TODO Create EntityManagerFactory using Persistence. This enable you to create EntityManagers.
    }

    //TODO remember to close all entityManagers

    @Override
    public List<Person> findAll() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Person> query = em.createQuery("FROM person", Person.class);
        List<Person> people = query.getResultList();
        em.close();
        return people;
    }

    @Override
    public Optional<Person> findById(int id) {
        EntityManager em = emf.createEntityManager();
        Person person = em.find(Person.class, id);
        em.close();
        return Optional.ofNullable(person);
    }

    @Override
    public int create(Person person) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try{
            transaction=em.getTransaction();
            transaction.begin();
            em.persist(person);
            transaction.commit();
        }catch (RuntimeException e){
            if (transaction!=null&&transaction.isActive()){
                transaction.rollback();
            }
        }
        em.close();
        return person.getId();
    }

    @Override
    public int update(Person person) {

        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try{
            transaction=em.getTransaction();
            transaction.begin();
            em.merge(person);
            transaction.commit();
        }catch (RuntimeException e){
            if (transaction!=null&&transaction.isActive()){
                transaction.rollback();
            }
        }
        em.close();
        return person.getId();

    }

    @Override
    public void delete(int id) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        Person person = em.find(Person.class, id);
        try{
            transaction=em.getTransaction();
            transaction.begin();
            em.remove(person);
            transaction.commit();
        }catch (RuntimeException e){
            if (transaction!=null&&transaction.isActive()){
                transaction.rollback();
            }
        }
        em.close();
    }

    @Override
    public void deleteAll()  {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction=null;

        try{
            transaction=em.getTransaction();
            transaction.begin();
            TypedQuery<Person> query = em.createQuery("FROM person", Person.class);
            List<Person> people = query.getResultList();
            for (Person a: people) {
                em.remove(a);
            }
            transaction.commit();
        }catch (RuntimeException e){
            if (transaction!=null&&transaction.isActive()){
                transaction.rollback();
            }
        }
        em.close();
    }
}
