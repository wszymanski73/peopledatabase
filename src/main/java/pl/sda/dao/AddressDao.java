package pl.sda.dao;

import pl.sda.domain.Address;

import java.util.List;
import java.util.Optional;

public interface AddressDao {

    List<Address> findAll();

    Optional<Address> findById(int id);

    int create(Address student);

    int update(Address student);

    void delete(int id);

    void deleteAll();
}
