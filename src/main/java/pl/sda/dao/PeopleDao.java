package pl.sda.dao;

import pl.sda.domain.Person;

import java.util.List;
import java.util.Optional;

public interface PeopleDao {

    List<Person> findAll();

    Optional<Person> findById(int id);

    int create(Person person);

    int update(Person person);

    void delete(int id);

    void deleteAll();
}
