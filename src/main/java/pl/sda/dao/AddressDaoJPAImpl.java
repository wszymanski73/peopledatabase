package pl.sda.dao;

import org.apache.commons.lang3.NotImplementedException;
import pl.sda.domain.Address;

import javax.persistence.*;
import java.util.List;
import java.util.Optional;

public class AddressDaoJPAImpl implements AddressDao {

    private static final EntityManagerFactory emf;

    static {
        emf = Persistence.createEntityManagerFactory("jpa_people");//TODO Create EntityManagerFactory using Persistence. This enable you to create EntityManagers.
    }

    //TODO remember to close all entityManagers

    @Override
    public List<Address> findAll() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Address> query = em.createQuery("FROM address",Address.class);
        List<Address> addresses = query.getResultList();
        em.close();
        return addresses;
    }

    @Override
    public Optional<Address> findById(int id) {
        EntityManager em = emf.createEntityManager();
        Address student = em.find(Address.class, id);
        em.close();
        return Optional.ofNullable(student);
    }

    @Override
    public int create(Address address) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try{
            transaction=em.getTransaction();
            transaction.begin();
            em.persist(address);
            transaction.commit();
        }catch (RuntimeException e){
            if (transaction!=null&&transaction.isActive()){
                transaction.rollback();
            }
        }
        em.close();
        return address.getId();
    }

    @Override
    public int update(Address address) {

        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try{
            transaction=em.getTransaction();
            transaction.begin();
            em.merge(address);
            transaction.commit();
        }catch (RuntimeException e){
            if (transaction!=null&&transaction.isActive()){
                transaction.rollback();
            }
        }
        em.close();
        return address.getId();

    }

    @Override
    public void delete(int id) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        Address student = em.find(Address.class, id);
        try{
            transaction=em.getTransaction();
            transaction.begin();
            em.remove(student);
            transaction.commit();
        }catch (RuntimeException e){
            if (transaction!=null&&transaction.isActive()){
                transaction.rollback();
            }
        }
        em.close();
    }

    @Override
    public void deleteAll()  {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction=null;

        try{
            transaction=em.getTransaction();
            transaction.begin();
            TypedQuery<Address> query = em.createQuery("FROM address",Address.class);
            List<Address> students = query.getResultList();
            for (Address a:students) {
                em.remove(a);
            }
            transaction.commit();
        }catch (RuntimeException e){
            if (transaction!=null&&transaction.isActive()){
                transaction.rollback();
            }
        }
        em.close();
    }
}
