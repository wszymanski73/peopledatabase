package pl.sda.dao;

import lombok.extern.java.Log;
import pl.sda.configuration.JdbcConnectionManager;
import pl.sda.domain.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log
public class PeopleDaoJDBCImpl implements PeopleDao {

    private static final String QUERY_ALL = "SELECT person.id, person.name, person.surname, person.age, person.height, person.weight FROM person,address where person.id=address.id";
    private static final String QUERY_BY_ID = "SELECT id, name, surname, age, height, weight FROM person WHERE id = ?";
    private static final String INSERT = "INSERT INTO person(name, surname, age, height, weight) VALUES(?,?,?,?,?)";
    private static final String UPDATE = "UPDATE person SET name = ?, surname = ?, age = ?, height = ?, weight = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM person WHERE id = ?";
    private static final String DELETE_ALL = "DELETE FROM person";

    private final JdbcConnectionManager jdbcConnectionManager;

    public PeopleDaoJDBCImpl(final JdbcConnectionManager jdbcConnectionManager) {
        this.jdbcConnectionManager = jdbcConnectionManager;
    }

    @Override
    public List<Person> findAll() {
        List<Person> people = new ArrayList<>();

        try (Connection conn = jdbcConnectionManager.getConnection();
             Statement stm = conn.createStatement()) {

            ResultSet resultSet = stm.executeQuery(QUERY_ALL);
            while (resultSet.next()) {
                Optional<Person> person = mapFromResultSet(resultSet);
                person.ifPresent(people::add);
            }

        } catch (SQLException e) {
            log.warning(e.getMessage());
        }

        return people;
    }

    @Override
    public Optional<Person> findById(int id) {
        try (Connection conn = jdbcConnectionManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(QUERY_BY_ID)) {
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return mapFromResultSet(rs);
            }
        } catch (SQLException ex) {
            log.warning(ex.getMessage());
        }

        return Optional.empty();
    }

    @Override
    public int create(Person person) {
        try (Connection conn = jdbcConnectionManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {

            ps.setString(1, person.getName());
            ps.setString(2, person.getSurname());
            ps.setInt(3, person.getAge());
            ps.setInt(4, person.getHeight());
            ps.setDouble(5, person.getWeight());
            ps.setInt(5, person.getId());

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 1) {
                ResultSet generatedKeys = ps.getGeneratedKeys();
                if (generatedKeys.next()) {
                    return generatedKeys.getInt(1);
                }
            }
        } catch (SQLException ex) {
            log.warning(ex.getMessage());
        }

        throw new IllegalStateException("Unable to create person");
    }

    @Override
    public int update(Person person) {
        try (Connection conn = jdbcConnectionManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(UPDATE)) {

            ps.setString(1, person.getName());
            ps.setString(2, person.getSurname());
            ps.setInt(3, person.getAge());
            ps.setInt(4, person.getHeight());
            ps.setDouble(5, person.getWeight());

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 1) {
                return person.getId();
            }
        } catch (SQLException ex) {
            log.warning(ex.getMessage());
        }

        throw new IllegalStateException("Unable to update person");
    }

    @Override
    public void delete(int id) {
        try (Connection conn = jdbcConnectionManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(DELETE)) {

            ps.setInt(1, id);
            ps.executeUpdate();

        } catch (SQLException ex) {
            log.warning(ex.getMessage());
        }
    }

    private Optional<Person> mapFromResultSet(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        String surname = rs.getString("surname");
        int age = rs.getInt("age");
        int height = rs.getInt("height");
        int weight = rs.getInt("weight");


        Person person = new Person();
        person.setId(id);
        person.setName(name);
        person.setSurname(surname);
        person.setAge(age);
        person.setHeight(height);
        person.setWeight((double) weight);
        return Optional.of(person);
    }

    @Override
    public void deleteAll() {
        try (Connection conn = jdbcConnectionManager.getConnection();
             Statement ps = conn.createStatement()) {

            ps.executeUpdate(DELETE_ALL);

        } catch (SQLException ex) {
            log.warning(ex.getMessage());
        }
    }
}
