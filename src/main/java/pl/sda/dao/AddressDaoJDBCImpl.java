package pl.sda.dao;

import lombok.extern.java.Log;
import pl.sda.configuration.JdbcConnectionManager;
import pl.sda.domain.Address;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log
public class AddressDaoJDBCImpl implements AddressDao {

    private static final String QUERY_ALL = "SELECT id, city, zipCode,street,homeNumber FROM address";
    private static final String QUERY_BY_ID = "SELECT id, city, zipCode,street,homeNumber FROM address WHERE id = ?";
    private static final String INSERT = "INSERT INTO address(city, zipCode,street,homeNumber) VALUES(?,?,?,?)";
    private static final String UPDATE = "UPDATE address SET city = ?, zipCode = ?, street = ?, homenumber = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM address WHERE Address_id = ?";
    private static final String DELETE_ALL = "DELETE FROM address";

    private final JdbcConnectionManager jdbcConnectionManager;

    public AddressDaoJDBCImpl(final JdbcConnectionManager jdbcConnectionManager) {
        this.jdbcConnectionManager = jdbcConnectionManager;
    }

    @Override
    public List<Address> findAll() {
        List<Address> addresss = new ArrayList<>();

        try (Connection conn = jdbcConnectionManager.getConnection();
             Statement stm = conn.createStatement()) {

            ResultSet resultSet = stm.executeQuery(QUERY_ALL);
            while (resultSet.next()) {
                Optional<Address> address = mapFromResultSet(resultSet);
                address.ifPresent(addresss::add);
            }

        } catch (SQLException e) {
            log.warning(e.getMessage());
        }

        return addresss;
    }

    @Override
    public Optional<Address> findById(int id) {
        try (Connection conn = jdbcConnectionManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(QUERY_BY_ID)) {
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return mapFromResultSet(rs);
            }
        } catch (SQLException ex) {
            log.warning(ex.getMessage());
        }

        return Optional.empty();
    }

    @Override
    public int create(Address address) {
        try (Connection conn = jdbcConnectionManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {

            ps.setString(1, address.getCity());
            ps.setString(2, address.getZipCode());
            ps.setString(3, address.getStreet());
            ps.setInt(4, address.getHomeNumber());

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 1) {
                ResultSet generatedKeys = ps.getGeneratedKeys();
                if (generatedKeys.next()) {
                    return generatedKeys.getInt(1);
                }
            }
        } catch (SQLException ex) {
            log.warning(ex.getMessage());
        }

        throw new IllegalStateException("Unable to create Address");
    }

    @Override
    public int update(Address address) {
        try (Connection conn = jdbcConnectionManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(UPDATE)) {

            ps.setString(1, address.getCity());
            ps.setString(2, address.getZipCode());
            ps.setString(3, address.getStreet());
            ps.setInt(4, address.getHomeNumber());
            ps.setInt(5, address.getId());

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 1) {
                return address.getId();
            }
        } catch (SQLException ex) {
            log.warning(ex.getMessage());
        }

        throw new IllegalStateException("Unable to update Address");
    }

    @Override
    public void delete(int id) {
        try (Connection conn = jdbcConnectionManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(DELETE)) {

            ps.setInt(1, id);
            ps.executeUpdate();

        } catch (SQLException ex) {
            log.warning(ex.getMessage());
        }
    }

    private Optional<Address> mapFromResultSet(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String city = rs.getString("City");
        String zipCode = rs.getString("zipCode");
        String street = rs.getString("street");
        int homenumber = rs.getInt("Homenumber");

        Address Address = new Address();
        Address.setId(id);
        Address.setCity(city);
        Address.setZipCode(zipCode);
        return Optional.of(Address);
    }

    @Override
    public void deleteAll() {
        try (Connection conn = jdbcConnectionManager.getConnection();
             Statement ps = conn.createStatement()) {

            ps.executeUpdate(DELETE_ALL);

        } catch (SQLException ex) {
            log.warning(ex.getMessage());
        }
    }
}
