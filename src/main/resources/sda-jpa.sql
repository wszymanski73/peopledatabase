INSERT INTO person(name, surname, age, height, weight) VALUES ('Adam','Adamski', 20,110,2.2,1), 
								('Jan','Mizerski', 21,120,2.3,2), 
								('Alicja','Adamska', 22,130,2.4,3), 
								('Karolina','Wolska', 23,140,2.5,4);
INSERT INTO address(city, zipCode, street, homeNumber) VALUES ('Katowice','41-100', 'Kosciuszki',110),
								('Chorzow','41-300', 'Makow',20),
								('Tychy','41-500', 'Bielska',1),
								('Myslowice','41-400', 'Mickiewicza',22);