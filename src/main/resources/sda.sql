DROP schema if exists people_jpa;
create schema people_jpa;
use people_jpa;

DROP table if exists person;
create table person
(
  id int auto_increment
    	primary key,
	name varchar(255) not null,
	surname varchar(255) not null,
    	age int not null,
	height int not null,
	weight double not null,
    addressid int, 
	FOREIGN KEY (addressid) REFERENCES address(id)
);

INSERT INTO person(name, surname, age, height, weight) VALUES ('Adam','Adamski', 20,110,2.2,1), 
								('Jan','Mizerski', 21,120,2.3,2), 
								('Alicja','Adamska', 22,130,2.4,3), 
								('Karolina','Wolska', 23,140,2.5,4); 

DROP table if exists address;
create table address
(
  id int auto_increment
    	primary key,
	city varchar(255) not null,
	zipCode varchar(255) not null,
	street varchar(255) not null,
    	homeNumber int not null;    
);

INSERT INTO address(city, zipCode, street, homeNumber) VALUES ('Katowice','41-100', 'Kosciuszki',110),
								('Chorzow','41-300', 'Makow',20),
								('Tychy','41-500', 'Bielska',1),
								('Myslowice','41-400', 'Mickiewicza',22);