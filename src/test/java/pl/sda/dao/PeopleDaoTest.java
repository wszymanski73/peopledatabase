package pl.sda.dao;

import org.junit.jupiter.api.Test;
import pl.sda.domain.Person;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

abstract class PeopleDaoTest {

    protected PeopleDao sut;

    @Test
    void findAll() {
        List<Person> personList = sut.findAll();

        assertThat(personList).hasSize(4);

       // Set<Integer> allAges = personList.stream().map(Person::getAge).collect(Collectors.toSet());
       // assertThat(allAges).containsExactlyInAnyOrder(20, 21, 22, 23);
    }

    @Test
    void findById() {
        Person existingPerson = findOneOfPerson();
        int id = existingPerson.getId();

        Optional<Person> person = sut.findById(id);

        assertThat(person).isPresent();
        assertThat(person.get().getAge()).isEqualTo(22);
        assertThat(person.get().getName()).isEqualTo("student 4");
    }

    @Test
    void create() {
        Person person = new Person();
        person.setName("SDA Student");
        person.setAge(4);

        int id = sut.create(person);

        Optional<Person> createdPerson = sut.findById(id);
        assertThat(createdPerson).isPresent();
        assertThat(createdPerson.get().getAge()).isEqualTo(4);
        assertThat(createdPerson.get().getName()).isEqualTo("SDA Student");
    }

    @Test
    void update() {
        Person existingPerson = findOneOfPerson();
        existingPerson.setName("some new name");

        int updatedPersonId = sut.update(existingPerson);

        Optional<Person> updatedPerson = sut.findById(updatedPersonId);
        assertThat(updatedPerson).isPresent();
        assertThat(updatedPerson.get().getAge()).isEqualTo(22);
        assertThat(updatedPerson.get().getName()).isEqualTo("some new name");
    }

    @Test
    void delete() {
        Person existingPerson = findOneOfPerson();
        int id = existingPerson.getId();

        sut.delete(id);

        Optional<Person> removedPerson = sut.findById(id);
        assertThat(removedPerson).isNotPresent();
    }

    private Person findOneOfPerson() {
        Optional<Person> existingPerson = sut.findAll()
                .stream()
                .filter(person -> person.getAge() == 22)
                .findFirst();
        assertThat(existingPerson).isPresent();
        return existingPerson.get();
    }

    @Test
    void deleteAll() {
        sut.deleteAll();
        List<Person> personList = sut.findAll();

        assertThat(personList).hasSize(0);
    }
}
