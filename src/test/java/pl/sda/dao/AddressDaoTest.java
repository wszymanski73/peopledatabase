package pl.sda.dao;

import org.junit.jupiter.api.Test;
import pl.sda.domain.Address;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

abstract class AddressDaoTest {

    protected AddressDao sut;

    @Test
    void findAll() {
        List<Address> students = sut.findAll();

        assertThat(students).hasSize(5);

        Set<Integer> allAges = students.stream().map(Address::getHomeNumber).collect(Collectors.toSet());
        assertThat(allAges).containsExactlyInAnyOrder(110, 20, 1, 22);
    }

    @Test
    void findById() {
        Address existingAddress = findOneOfAddress();
        int id = existingAddress.getId();

        Optional<Address> address = sut.findById(id);

        assertThat(address).isPresent();
        assertThat(address.get().getHomeNumber()).isEqualTo(10);
        assertThat(address.get().getCity()).isEqualTo("Tychy");
    }

    @Test
    void create() {
        Address address = new Address();
        address.setCity("Sosnowiec");
        address.setZipCode("41-200");
        address.setStreet("Ostrogorska");
        address.setHomeNumber(12);

        int id = sut.create(address);

        Optional<Address> createdAddress = sut.findById(id);
        assertThat(createdAddress).isPresent();
        assertThat(createdAddress.get().getCity()).isEqualTo("Sosnowiec");
        assertThat(createdAddress.get().getZipCode()).isEqualTo("41-200");
        assertThat(createdAddress.get().getStreet()).isEqualTo("Ostrogorska");
        assertThat(createdAddress.get().getZipCode()).isEqualTo(12);
    }

    @Test
    void update() {
        Address existingAddress = findOneOfAddress();
        existingAddress.setCity("Opole");

        int updatedAddressId = sut.update(existingAddress);

        Optional<Address> updatedAddress = sut.findById(updatedAddressId);
        assertThat(updatedAddress).isPresent();
        assertThat(updatedAddress.get().getCity()).isEqualTo("Opole");
    }

    @Test
    void delete() {
        Address existingAddress = findOneOfAddress();
        int id = existingAddress.getId();

        sut.delete(id);

        Optional<Address> removedAddress = sut.findById(id);
        assertThat(removedAddress).isNotPresent();
    }

    private Address findOneOfAddress() {
        Optional<Address> existingStudent = sut.findAll()
                .stream()
                .filter(student -> student.getHomeNumber() == 22)
                .findFirst();
        assertThat(existingStudent).isPresent();
        return existingStudent.get();
    }

    @Test
    void deleteAll() {
        sut.deleteAll();
        List<Address> students = sut.findAll();

        assertThat(students).hasSize(0);
    }
}
